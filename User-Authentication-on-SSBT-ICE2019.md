# User Authentication on SSBT for ICE 2019

Abbreviation | Description
------------ | -------------
BT           | Betting Terminal
NPS          | Novomatic Prime Sports
OMEGA IPS    | OMEGA IPS API hosted by OMEGA Player Service Server
SSBT         | Self Service Betting Terminal
PIN          | 4 - 6 digit Pin code, equivlent to player's password
PartyID      | Unique identifier (PK) in OMEGA System, once created, can NOT be changed
UUID         | Unique identifier from 3rd party system. UUID can be of any format in string, numbers, letters etc. UUID can only be assigned to one player, same UUID can not be reassigned to a different player. However, we could potentially allow 3rd party system to update the UUID if required. In this case barcode can be stored in UUID field.
Username     | Also known as UserID, player can pick a Username and password for them to login the OMEGA system via ips/login API. Username will be used by web and mobile, hence CANNOT be numeric only

> IPS LoginByBrand will create player if not exists, then login player based in brandId, brandPassword currency and UUID provided
> Possible validation using UUID + PIN, in that case OMEGA Password policy need to be able to support simple digit only password

```mermaid
sequenceDiagram
    NPS BT ->> User: Prints Barcodes (UUID)
    User -->> NPS BT: Barcode
    User ->> BT : Scan Barcode
    BT ->> OMEGA IPS: IPS LoginByBrand
    OMEGA IPS -->> BT: Balance Response
    BT -->> User : Logged In with Balance Displayed
```