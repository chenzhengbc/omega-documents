# Wallet Account Top-up through SSBT for ICE 2019

Abbreviation | Description
------------ | -------------
BT           | Betting Terminal
NPS          | Novomatic Prime Sports
OMEGA IPS    | OMEGA IPS API hosted by OMEGA Player Service Server
SSBT         | Self Service Betting Terminal
PIN          | 4 - 6 digit Pin code, equivlent to player's password
PartyID      | Unique identifier (PK) in OMEGA System, once created, can NOT be changed
UUID         | Unique identifier from 3rd party system. UUID can only be assigned to one player, same UUID can not be reassigned to a different player. However, we could potentially allow 3rd party system to update the UUID if required. In this case barcode can be stored in UUID field.
Username     | Also known as UserID, player can pick a Username and password for them to login the OMEGA system via ips/login API
PIN          | 4 - 6 digit Pin code, equivlent to player's password

TBD